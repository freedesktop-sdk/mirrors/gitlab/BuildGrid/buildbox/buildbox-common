// Copyright 2024 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <fstream>

#include <google/protobuf/util/json_util.h>

#include <buildboxcommon_logging.h>
#include <buildboxcommon_protojsonutils.h>

#include <rumbad_filewriter.h>

namespace rumbad {

FileWriter::FileWriter(std::string filepath) : d_logFilepath(filepath) {}

void FileWriter::write(const std::vector<Message> &messages)
{
    google::protobuf::util::JsonPrintOptions options;
    buildboxcommon::jsonOptionsAlwaysPrintFieldsWithNoPresence(options);

    std::ofstream logfile(this->d_logFilepath,
                          std::ofstream::app | std::ofstream::out);

    if (logfile) {
        for (const auto &message : messages) {
            std::string json;
            auto status = google::protobuf::util::MessageToJsonString(
                message.data, &json, options);
            if (!status.ok()) {
                BUILDBOX_LOG_WARNING("Failed to write to log file: " +
                                     this->d_logFilepath);
                return;
            }
            logfile << json << std::endl;
        }
    }
    else {
        BUILDBOX_LOG_WARNING("Failed to write to log file: " +
                             this->d_logFilepath);
    }
}

} // namespace rumbad
