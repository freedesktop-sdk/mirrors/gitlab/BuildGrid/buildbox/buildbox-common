// Copyright 2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INCLUDED_RUMBAD_QUEUE
#define INCLUDED_RUMBAD_QUEUE

#include <mutex>
#include <queue>

namespace rumbad {

template <typename T> class Queue {
  public:
    std::pair<T, bool> pop()
    {
        T value;
        bool success = false;
        std::lock_guard<std::mutex> lock(this->queueAccessLock);
        if (!this->queue.empty()) {
            value = this->queue.front();
            this->queue.pop();
            success = true;
        }
        return std::make_pair(value, success);
    };

    void push(T value)
    {
        std::lock_guard<std::mutex> lock(this->queueAccessLock);
        this->queue.push(value);
    }

    int size()
    {
        std::lock_guard<std::mutex> lock(this->queueAccessLock);
        return this->queue.size();
    }

  private:
    std::queue<T> queue;
    std::mutex queueAccessLock;
};

} // namespace rumbad

#endif
