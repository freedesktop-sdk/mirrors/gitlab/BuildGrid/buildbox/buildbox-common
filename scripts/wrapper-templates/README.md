# Templates for wrappers and configurations

Use `envsubst` command to generate wrappers and configuration files

For example, if `recc` and other `buildbox` tools are installed under `/opt`:

```bash
RECC=/opt/bin/recc COMMAND=/usr/bin/c++ envsubst < recc-wrapper.in > recc-c++
RECC=/opt/bin/recc COMMAND=/usr/bin/cc envsubst < recc-wrapper.in > recc-cc
```

Similarly, to generate `recc.conf`:

```bash
RECC_CONFIG_DIRECTORY="\"${RECC_CONFIG_DIRECTORY}\"" \
  RECC_CONFIG_PREFIX="/opt/etc" \
  RECC_SERVER="unix:///opt/var/run/casd/casd.sock" \
  RECC_INSTANCE="local-server" \
  RECC_REMOTE_PLATFORM_ISA="x86_64" \
  RECC_REMOTE_PLATFORM_OSFamily="Linux" \
  RECC_REMOTE_PLATFORM_OSRelease="RHEL 8.10" \
  envsubst < recc.conf.in > recc.conf
```

The first line `RECC_CONFIG_DIRECTORY="\"${RECC_CONFIG_DIRECTORY}` is to ensure
`RECC_CONFIG_DIRECTORY` does not get substituted.

## Using `make` to generate wrappers and configurations files

```bash
make
```
