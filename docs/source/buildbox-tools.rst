.. _buildbox-tools:

Buildbox tools
==============

Buildbox repository contains several tools that can be used with BuildBox, BuildGrid, and other Remote Execution services:

  * ``casdownload``: downloads blobs, action outputs and directory trees from a CAS server

  * ``chrootbuilder``: generates filesystem images from a docker container or image

  * ``logstreamreceiver``: listens for LogStream API ``ByteStream.Write()`` requests and echoes the data it receives to stdout

  * ``logstreamtail``: issues LogStream API ``ByteStream.Read()`` requests and prints the data it reads to stdout

  * ``outputstreamer``: reads from stdin and streams it to the given ByteStream endpoint
