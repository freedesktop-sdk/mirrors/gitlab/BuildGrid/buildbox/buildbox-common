Resources
=========

Below are links to some potentially useful resources for working in the
remote execution space:

-  `BuildBox GitLab repositories`_
-  `BuildBox issue tracker`_
-  `Mailing list`_
-  `Slack invite link`_ (channel: #buildbox)
-  `FUSE wikipedia entry`_
-  `Bubblewrap GitHub repository`_
-  `Userchroot GitHub repository`_
-  `About BuildGrid`_
-  `BuildGrid GitLab repository`_
-  `REAPI design document`_
-  `REAPI protobuf specification`_
-  `RWAPI design document`_
-  `RWAPI protobuf specification`_
-  `Bazel remote caching and execution documentation`_
-  `RECC usage instructions and GitLab repository`_
-  `BuildStream GitLab repository`_
-  `BuildStream documentation`_

.. _BuildBox GitLab repositories: https://gitlab.com/BuildGrid/buildbox
.. _BuildBox issue tracker: https://gitlab.com/groups/BuildGrid/buildbox/-/boards
.. _Mailing list: https://lists.buildgrid.build/cgi-bin/mailman/listinfo/buildgrid
.. _Slack invite link: https://bit.ly/2SG1amT
.. _FUSE wikipedia entry: https://en.wikipedia.org/wiki/Filesystem_in_Userspace
.. _Bubblewrap GitHub repository: https://github.com/containers/bubblewrap
.. _Userchroot GitHub repository: https://github.com/bloomberg/userchroot
.. _About BuildGrid: https://buildgrid.gitlab.io/buildgrid/user/about.html
.. _BuildGrid GitLab repository: https://gitlab.com/BuildGrid/buildgrid
.. _REAPI design document: https://docs.google.com/document/d/1AaGk7fOPByEvpAbqeXIyE8HX_A3_axxNnvroblTZ_6s/edit?usp=sharing
.. _REAPI protobuf specification: https://github.com/bazelbuild/remote-apis/blob/master/build/bazel/remote/execution/v2/remote_execution.proto
.. _RWAPI design document: https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU/edit?usp=sharing
.. _RWAPI protobuf specification: https://github.com/googleapis/googleapis/blob/master/google/devtools/remoteworkers/v1test2/bots.proto
.. _Bazel remote caching and execution documentation: https://docs.bazel.build/versions/master/remote-caching.html
.. _RECC usage instructions and GitLab repository: https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/recc/README.md
.. _BuildStream GitLab repository: https://gitlab.com/BuildStream
.. _BuildStream documentation: https://buildstream.build/
