.. _buildbox-common:

buildbox-common
===============

https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/buildbox-common

``buildbox-common`` is a library that provides functions needed to implement solutions around the Remote Execution API. For example:

* Interacting with a CAS server (also supporting the LocalCas protocol)
* Parsing CLI options
* Handling of gRPC connections
* Manipulating files on disk
* Logging
* Remote Execution API (REAPI) protobufs
* Creating and manipulating REAPI directory representations
* Creating temporary files and directories
* Parsing and manipulating timestamps
