/*
 * Copyright 2024 Bloomberg LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_tomlutils.h>

namespace buildboxcommon {

std::string formatAccessPath(const AccessPath &accessPath)
{

    std::stringstream ss;
    for (const auto &p : accessPath) {
        ss << "[";
        if (const std::string *key = std::get_if<std::string>(&p)) {
            ss << *key;
        }
        if (const size_t *index = std::get_if<size_t>(&p)) {
            ss << *index;
        }
        ss << "]";
    }
    return ss.str();
}

void TOMLUtils::throwTOMLTypeError(const toml::node &node,
                                   const toml::node_type expectedType,
                                   AccessPath &accessPath,
                                   const std::string &message)
{
    const toml::source_region &source = node.source();
    std::stringstream ss;
    ss << "Invalid TOML input. Begin " << source.begin
       << ", end: " << source.end;
    ss << ", path: " << formatAccessPath(accessPath);
    ss << ", expected type: " << expectedType;
    if (!message.empty()) {
        ss << ", " << message;
    }

    throw std::invalid_argument(ss.str());
}

} // namespace buildboxcommon
