#ifndef INCLUDED_BUILDBOXCOMMON_DEPS_H
#define INCLUDED_BUILDBOXCOMMON_DEPS_H

//
// Do not include.
//
// A list of commonly-included headers, to be compiled into a precompiled
// header. See the library 'pch' in top CMakeLists.txt.
//

#include <algorithm>
#include <any>
#include <array>
#include <atomic>
#include <cctype>
#include <cerrno>
#include <chrono>
#include <cmath>
#include <condition_variable>
#include <csignal>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <exception>
#include <fstream>
#include <functional>
#include <initializer_list>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <random>
#include <regex>
#include <set>
#include <shared_mutex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <system_error>
#include <thread>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <variant>
#include <vector>

#include <arpa/inet.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fnmatch.h>
#include <libgen.h>
#include <math.h>
#include <netdb.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#ifdef __linux__
#include <sys/prctl.h>
#endif
#include <sys/resource.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#include <build/bazel/remote/asset/v1/remote_asset.grpc.pb.h>
#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>
#include <build/bazel/remote/logstream/v1/remote_logstream.grpc.pb.h>
#include <build/buildbox/execution_stats.grpc.pb.h>
#include <build/buildbox/execution_stats.pb.h>
#include <build/buildgrid/local_cas.grpc.pb.h>
#include <google/bytestream/bytestream.grpc.pb.h>
#include <google/longrunning/operations.grpc.pb.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/util/json_util.h>
#include <google/protobuf/util/message_differencer.h>
#include <google/protobuf/util/time_util.h>
#include <google/rpc/code.pb.h>
#include <google/rpc/error_details.grpc.pb.h>
#include <google/rpc/status.pb.h>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/generic/async_generic_service.h>
#include <grpcpp/grpcpp.h>
#include <grpcpp/security/credentials.h>

#include <glog/logging.h>
#include <openssl/evp.h>

#endif
