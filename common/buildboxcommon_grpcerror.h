// Copyright 2021 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INCLUDED_BUILDBOXCOMMON_GRPCERROR
#define INCLUDED_BUILDBOXCOMMON_GRPCERROR

#include <grpcpp/grpcpp.h>

#include <string>

namespace buildboxcommon {

class GrpcError : public std::runtime_error {
  public:
    static void throwGrpcError(const grpc::Status &errorStatus)
    {
        throw GrpcError(std::to_string(errorStatus.error_code()) + ": " +
                            errorStatus.error_message(),
                        errorStatus);
    }

    explicit GrpcError(const char *message, const grpc::Status &_status)
        : std::runtime_error(message), status(_status)
    {
    }
    explicit GrpcError(const std::string &message, const grpc::Status &_status)
        : std::runtime_error(message), status(_status)
    {
    }

    grpc::Status status;
};

} // namespace buildboxcommon

#endif
