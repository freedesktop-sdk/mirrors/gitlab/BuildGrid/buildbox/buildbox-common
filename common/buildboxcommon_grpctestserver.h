/*
 * Copyright 2016 gRPC authors.
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCOMMON_GRPCTESTSERVER
#define INCLUDED_BUILDBOXCOMMON_GRPCTESTSERVER

#include <memory>

#include <google/protobuf/message.h>
#include <google/protobuf/util/message_differencer.h>
#include <grpcpp/generic/async_generic_service.h>
#include <grpcpp/grpcpp.h>
#include <gtest/gtest.h>

namespace buildboxcommon {

/**
 * An in-process gRPC server to test client code. This serves a similar purpose
 * as mock client stubs, however, this server works with both sync and async
 * client code.
 *
 * Construct a `GrpcTestServerContext` instance for each to be mocked gRPC
 * method call. As the `GrpcTestServerContext` methods are blocking, a
 * separate thread should be used.
 */
class GrpcTestServer {
  public:
    GrpcTestServer()
    {
        grpc::ServerBuilder builder;
        builder.RegisterAsyncGenericService(&d_genericService);
        builder.AddListeningPort("localhost:0",
                                 grpc::InsecureServerCredentials(), &d_port);
        d_cq = builder.AddCompletionQueue();
        d_server = builder.BuildAndStart();
        d_channel = d_server->InProcessChannel(grpc::ChannelArguments());
    }

    virtual ~GrpcTestServer() { d_server->Shutdown(); }

    /**
     * Return the in-process gRPC channel, which test clients can use to
     * call gRPC methods on the test server.
     */
    std::shared_ptr<grpc::Channel> channel() { return d_channel; }

    /**
     * Return the URL, which out-of-process test clients can use to
     * call gRPC methods on the test server.
     */
    std::string url() { return "http://localhost:" + std::to_string(d_port); }

    friend class GrpcTestServerContext;

  private:
    std::unique_ptr<grpc::Server> d_server;
    std::unique_ptr<grpc::ServerCompletionQueue> d_cq;
    std::shared_ptr<grpc::Channel> d_channel;
    grpc::AsyncGenericService d_genericService;
    int d_port;
};

class GrpcTestServerContext {
  public:
    // Allow access to underlying context to enable inspecting/returning
    // metadata and other per-request information from tests
    grpc::GenericServerContext d_ctx;

    /**
     * Create a server context and wait for the client to call the specified
     * method. The method name must be fully qualified with the package name
     * and the service name.
     */
    GrpcTestServerContext(GrpcTestServer *server, const std::string &method)
        : d_server(server), d_stream(&d_ctx)
    {
        d_cq = d_server->d_cq.get();
        d_server->d_genericService.RequestCall(&d_ctx, &d_stream, d_cq, d_cq,
                                               nullptr);
        next();
        EXPECT_EQ(d_ctx.method(), method);
    }

    void next()
    {
        void *tag;
        bool ok;
        EXPECT_TRUE(d_cq->Next(&tag, &ok) && ok);
    }

    /**
     * Wait for and read a message from the client and verify that it matches
     * the expected message.
     */
    template <typename R>
    void
    read(const R &expectedRequest,
         google::protobuf::util::MessageDifferencer::RepeatedFieldComparison
             repeatedFieldComparison = google::protobuf::util::
                 MessageDifferencer::RepeatedFieldComparison::AS_LIST)
    {
        grpc::ByteBuffer inBuffer;
        R inMsg;
        d_stream.Read(&inBuffer, nullptr);
        next();
        EXPECT_TRUE(parseFromByteBuffer(&inBuffer, &inMsg));

        std::string diff;
        google::protobuf::util::MessageDifferencer differencer;
        differencer.set_repeated_field_comparison(repeatedFieldComparison);
        differencer.ReportDifferencesToString(&diff);
        if (!differencer.Compare(expectedRequest, inMsg)) {
            FAIL() << "Unexpected differences in received request:\n" << diff;
        }
    }

    /**
     * Send a message back to the client and keep the stream open.
     */
    template <typename W> void write(const W &response)
    {
        auto outBuffer = serializeToByteBuffer(response);
        d_stream.Write(*outBuffer, nullptr);
        next();
    }

    /**
     * Send a message back to the client and finish the stream with an
     * `OK` status.
     */
    template <typename W> void writeAndFinish(const W &response)
    {
        auto outBuffer = serializeToByteBuffer(response);
        d_stream.WriteAndFinish(*outBuffer, grpc::WriteOptions(),
                                grpc::Status::OK, nullptr);
        next();
    }

    /**
     * Finish the stream with the specified status.
     */
    void finish(const grpc::Status &status)
    {
        d_stream.Finish(status, nullptr);
        next();
    }

    /**
     * Expect client cancelling.
     */
    void expectCancel()
    {
        void *tag;
        bool ok;
        d_stream.Finish(grpc::Status::OK, nullptr);
        EXPECT_TRUE(d_cq->Next(&tag, &ok));
        EXPECT_FALSE(ok);
    }

  private:
    static bool parseFromByteBuffer(grpc::ByteBuffer *buffer,
                                    google::protobuf::Message *message)
    {
        std::string s;
        std::vector<grpc::Slice> slices;
        buffer->Dump(&slices);
        for (const auto &slice : slices) {
            s.append(reinterpret_cast<const char *>(slice.begin()),
                     slice.size());
        }
        return message->ParseFromString(s);
    }

    static std::unique_ptr<grpc::ByteBuffer>
    serializeToByteBuffer(const google::protobuf::Message &message)
    {
        std::string buf;
        message.SerializeToString(&buf);
        grpc::Slice slice(buf);
        return std::make_unique<grpc::ByteBuffer>(&slice, 1);
    }

    GrpcTestServer *d_server;
    grpc::ServerCompletionQueue *d_cq;
    grpc::GenericServerAsyncReaderWriter d_stream;
};

} // namespace buildboxcommon

#endif
