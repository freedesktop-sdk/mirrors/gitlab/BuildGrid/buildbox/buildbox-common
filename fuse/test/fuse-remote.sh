#!/bin/bash

. $(dirname "$0")/fuse.sh
. $(dirname "$0")/fuse-basic.sh
. $(dirname "$0")/cas.sh

if [ "$#" -lt 1 ]; then
	echo "Missing test name" >&2
	exit 1
fi

umask 0022

TEST="$1"
shift

mkdir "$TMPDIR/cas-remote"

REF="$TMPDIR/reference"
mkdir "$REF"

# Set up the reference directory
base "$REF"
$TEST "$REF"

start_cas_server

# Create the base via FUSE
start_buildbox
base "$ROOT"

# Apply the test-specific changes via FUSE after restart
restart_buildbox
$TEST "$ROOT"

# Verify that the FUSE root matches the reference directory
compare_trees "$REF" "$ROOT"

# Verify that the FUSE root matches the reference directory
# after restart (save changes in CAS and reload them)
restart_buildbox
compare_trees "$REF" "$ROOT"

stop_buildbox

stop_cas_server
