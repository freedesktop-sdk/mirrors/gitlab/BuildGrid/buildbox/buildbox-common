/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXRUN_RUN_SUBPROCESS
#define INCLUDED_BUILDBOXRUN_RUN_SUBPROCESS

#include <filesystem>
#include <functional>
#include <memory>
#include <ostream>
#include <string>

#include <buildboxcommon_temporaryfile.h>

namespace buildboxcommon {
namespace buildboxrun {
namespace oci {
/* Executes a given command on the command line. The command is immediately
 * executed once the handle is constructed. If an stdout or stderr stream is
 * supplied, then the command itself cannot contain redirects to these streams.
 * Otherwise the behavior is undefined. */
class RunSubprocess {
  private:
    std::unique_ptr<TemporaryFile> stdoutLog;
    std::unique_ptr<TemporaryFile> stderrLog;

    static const std::string stdoutLogPrefix;
    static const std::string stderrLogPrefix;

  public:
    RunSubprocess(const std::filesystem::path &dir, const std::string &cmd,
                  std::ostream *out = nullptr, std::ostream *err = nullptr);

    ~RunSubprocess() = default;

    // The handle shouldn't be copyable/movable.
    RunSubprocess(const RunSubprocess &) = delete;
    RunSubprocess &operator=(const RunSubprocess &) = delete;
    RunSubprocess(RunSubprocess &&) = delete;
    RunSubprocess &operator=(RunSubprocess &&) = delete;
};
} // namespace oci
} // namespace buildboxrun
} // namespace buildboxcommon

#endif
