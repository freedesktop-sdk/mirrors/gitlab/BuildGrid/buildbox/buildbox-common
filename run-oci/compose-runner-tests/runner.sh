#!/usr/bin/env bash
set -eux -o pipefail

# Sleep to give buildgrid time to startup
sleep 1

casupload --cas-server=http://controller:50051 mock-data

/buildbox-e2e-base-build/buildbox-tools/cpp/build/rexplorer/rexplorer \
  --action=$(cat mock-data/outputs/action.digest) \
  --verbose \
  --pretty \
  --cas-remote=http://controller:50051

build/buildbox-run-oci \
  --action=mock-data/outputs/action.data \
  --remote=http://localhost:60061 \
  --verbose \
  --no-logs-capture \
  --log-level=debug \
  --action-result=/tmp/result
