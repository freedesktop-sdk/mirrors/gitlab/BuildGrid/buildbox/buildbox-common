# BuildBox-Run-OCI Platform Properties

## Supported Properties
        Property                                Description
```
chrootRootDigest=STRING             The root digest.
container-image=STRING              The image ID of the container image to be executed (MUST be in the format that includes its sha256 hash)
memory-limit=UINT64                 (default value set in the arguments to buildbox-run-oci command) 
                                    The maximum size to cap the process's virtual memory at, in bytes. Supported suffixes: KB, MB, GB.
cpu-time=UINT64                     (default value set in the arguments to buildbox-run-oci command) 
                                    The maximum amount of time to cap the process's CPU consumption at, in seconds. 
unixUID=INT                         (default: 1000) The user identifier to set in the container namespace.
unixGID=INT                         The group identifier to set in the container namespace.
```

## Supported Remote Asset API Properties 
Note: These properties are only processed if the Asset Client is set.

        Property                                Description
```                    
chrootAssetUrl=STRING               The URI of the asset to be executed.                    
chrootAssetQualifier=STRING         (format: qualifier_name=value) Qualifier for the asset.
```

