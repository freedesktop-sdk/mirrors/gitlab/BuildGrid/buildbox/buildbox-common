if (NOT DEFINED PROTOBUF_TARGET)
    message(FATAL_ERROR "import BuildboxCommonConfig.cmake before including BulidboxCommonProtoc")
endif()

#
# protoc_compile compiles .proto files, both with gRPC plugin and without it,
# with protobuf and buildbox .proto files in the include path.
#
# Arguments:
#  - PROTOS is a list of .proto file names, relative to source directory
#  - SOURCE_DIR is source directory, optional, defaults to CMAKE_CURRENT_SOURCE_DIR
#  - DEST_DIR is a directory for generated files, optional, defaults to CMAKE_CURRENT_BINARY_DIR
#
# Output variables:
#  - PROTO_GENERATED_SRCS is a list of generated source files' names
#
function(protoc_compile)
    find_program(PROTOC protoc)
    if (NOT PROTOC)
        message(FATAL_ERROR "protoc not found")
    endif()

    find_program(GRPC_CPP_PLUGIN grpc_cpp_plugin)
    if (NOT GRPC_CPP_PLUGIN)
        message(FATAL_ERROR "grpc_cpp_plugin not found")
    endif()

    cmake_parse_arguments("" "" "SOURCE_DIR;DEST_DIR" "PROTOS" ${ARGN})
    if("${_SOURCE_DIR}" STREQUAL "")
        set(src_dir ${CMAKE_CURRENT_SOURCE_DIR})
    else()
        set(src_dir ${_SOURCE_DIR})
    endif()
    if("${_DEST_DIR}" STREQUAL "")
        set(dest_dir ${CMAKE_CURRENT_BINARY_DIR})
    else()
        set(dest_dir ${_DEST_DIR})
    endif()

    set(args "--proto_path=${src_dir}")
    foreach(inc_dir ${PROTOBUF_INCLUDE_DIRS})
        list(APPEND args "--proto_path=${inc_dir}")
    endforeach()

    if(NOT "${BuildboxCommon_DIR}" STREQUAL "")
        # This file was installed and included from another package,
        # so we add BuildboxCommon .proto files to the path.
        get_target_property(common_proto_path
            Buildbox::buildboxcommon INTERFACE_INCLUDE_DIRECTORIES)
        list(APPEND args "--proto_path=${common_proto_path}")
    endif()

    foreach(proto ${_PROTOS})
        set(src_proto "${src_dir}/${proto}.proto")
        set(dest_cc "${dest_dir}/${proto}.pb.cc")
        set(dest_grpc_cc "${dest_dir}/${proto}.grpc.pb.cc")

        add_custom_command(OUTPUT ${dest_cc} "${dest_dir}/${proto}.pb.h"
            COMMAND ${PROTOC} ${args} "--cpp_out=${dest_dir}" ${src_proto}
            DEPENDS ${src_proto})

        add_custom_command(OUTPUT ${dest_grpc_cc} "${dest_dir}/${proto}.grpc.pb.h" "${dest_dir}/${proto}_mock.grpc.pb.h"
            COMMAND ${PROTOC} ${args}
            "--grpc_out=generate_mock_code=true:${dest_dir}"
            "--plugin=protoc-gen-grpc=${GRPC_CPP_PLUGIN}"
            ${src_proto}
            DEPENDS ${src_proto})

        list(APPEND gen_srcs ${dest_cc} ${dest_grpc_cc})
    endforeach()

    set(PROTO_GENERATED_SRCS ${gen_srcs} PARENT_SCOPE)
endfunction()
