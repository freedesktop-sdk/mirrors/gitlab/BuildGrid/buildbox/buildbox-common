// Copyright 2018 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <deps.h>
#include <env.h>
#include <fileutils.h>

#include <buildboxcommon_logging.h>

#include <cstring>
#include <iostream>
#include <regex>
#include <string>

using namespace recc;

const std::string HELP(
    "USAGE: deps<command>\n"
    "\n"
    "Attempts to determine the files needed to execute the given compiler\n"
    "command, then prints a newline-separated list of them.");

int main(int argc, char *argv[])
{
    const std::string programName = std::string(
        argv[0]); // NOLINT (cppcoreguidelines-pro-bounds-pointer-arithmetic)
    buildboxcommon::logging::Logger::getLoggerInstance().initialize(
        programName.c_str());
    std::vector<std::string> cliArgs(argv + 1, argv + argc);

    Env::set_config_locations();
    Env::parse_config_variables();
    const std::string cwd = FileUtils::getCurrentWorkingDirectory();

    if (argc <= 1 || cliArgs[0] == "--help" || cliArgs[0] == "-h") {
        BUILDBOX_LOG_WARNING(HELP);
        return 0;
    }
    try {
        const auto parsedCommand =
            ParsedCommandFactory::createParsedCommand(cliArgs, cwd.c_str());
        const auto deps = Deps::get_file_info(parsedCommand).d_dependencies;
        for (const auto &dep : deps) {
            BUILDBOX_LOG_INFO(dep);
        }
    }
    catch (const subprocess_failed_error &e) {
        exit(e.d_error_code);
    }
}
