/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <logstreamtail.h>

#include <buildboxcommon_logging.h>
#include <buildboxcommon_logstreamwriter.h>
#include <buildboxcommon_protos.h>

#include <grpc/grpc.h>

grpc::Status LogStreamTail::readLogStream(
    const buildboxcommon::ConnectionOptions &connectionOptions,
    const std::string &resourceName,
    const DataAvailableCallback &dataAvailableCallback)
{
    auto channel = connectionOptions.createChannel();
    std::shared_ptr<google::bytestream::ByteStream::StubInterface>
        byteStreamStub(google::bytestream::ByteStream::NewStub(channel));

    return readLogStream(byteStreamStub, resourceName, dataAvailableCallback);
}

grpc::Status LogStreamTail::readLogStream(
    const std::shared_ptr<google::bytestream::ByteStream::StubInterface>
        &byteStreamStub,
    const std::string &resourceName,
    const DataAvailableCallback &dataAvailableCallback)
{
    buildboxcommon::ReadRequest request;
    request.set_resource_name(resourceName);
    request.set_read_offset(0);

    grpc::ClientContext context;
    auto reader = byteStreamStub->Read(&context, request);

    buildboxcommon::ReadResponse response;
    while (reader->Read(&response)) {
        dataAvailableCallback(response.data());
    }

    const auto read_status = reader->Finish();
    BUILDBOX_LOG_INFO("Read finished with status: '"
                      << read_status.error_message() << "' (code "
                      << read_status.error_code() << ")");
    return read_status;
}
