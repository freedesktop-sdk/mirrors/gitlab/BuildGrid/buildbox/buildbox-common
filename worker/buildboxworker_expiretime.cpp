/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_expiretime.h>

#include <buildboxcommon_logging.h>

namespace buildboxworker {

std::chrono::system_clock::time_point
ExpireTime::convertToTimePoint(const google::protobuf::Timestamp &timestamp)
{
    std::chrono::system_clock::time_point time =
        std::chrono::system_clock::from_time_t(timestamp.seconds());
    const int64_t NANOSECONDS_PER_MICROSECONDS = 1000;
    time += std::chrono::microseconds(
        int64_t(timestamp.nanos() / NANOSECONDS_PER_MICROSECONDS));

    return time;
}

} // namespace buildboxworker
