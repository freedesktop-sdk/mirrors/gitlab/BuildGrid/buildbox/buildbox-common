#ifndef INCLUDED_CASDOWNLOAD_PROCESSARGS
#define INCLUDED_CASDOWNLOAD_PROCESSARGS

#include <buildboxcommon_casclient.h>
#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>
#include <string>

namespace casdownload {

enum DigestType { DIGEST_ROOT, DIGEST_TREE, DIGEST_ACTION, DIGEST_FILE };

struct ProcessedArgs {
    bool d_processed;
    bool d_valid;

    buildboxcommon::LogLevel d_logLevel;
    buildboxcommon::ConnectionOptions d_casConnectionOptions;

    std::string d_destinationDir;

    DigestType d_digestType;
    buildboxcommon::Digest d_digest;

    buildboxcommon::DigestFunction_Value d_digestFunctionValue;
};

ProcessedArgs processArgs(int argc, char *argv[]);

}; // namespace casdownload

#endif
