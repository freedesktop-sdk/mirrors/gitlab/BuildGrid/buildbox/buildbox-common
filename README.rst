What is BuildBox?
=================

BuildBox is a suite of tools for remote build execution, conforming to
the `Remote Execution API`_ and the `Remote Workers API`_. It is highly
configurable and extensible, and has support for Linux and Solaris platforms.

BuildBox works as the worker implementation for remote execution
services such as `BuildGrid`_, and integrates with clients such as
`BuildStream`_ for local builds and caching.

.. _Remote Execution API: https://github.com/bazelbuild/remote-apis
.. _Remote Workers API: https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU
.. _BuildGrid: https://gitlab.com/BuildGrid/buildgrid
.. _BuildStream: https://gitlab.com/BuildStream/buildstream

BuildBox consists of many tools, see the ``README`` files in subdirectories
for their specific documentation.

Installation
============

Dependencies
------------

buildbox uses

* CMake
* pkg-config
* ninja (if selected, for faster builds)
* GNU Make (if ninja is not selected)
* C++ compiler
* gRPC
* Protobuf
* GoogleTest
* GoogleBenchmark (only if benchmarking is enabled)
* glog
* OpenSSL
* zlib (only for ``recc``)
* nlohmann-json (only for ``recc`` and ``buildbox-run-oci``)

GNU/Linux
---------

**Debian/Ubuntu**

Install major dependencies along with some other packages through ``apt``::

    [sudo] apt-get install cmake g++ gcc googletest libgmock-dev libgoogle-glog-dev libssl-dev pkg-config uuid-dev nlohmann-json3-dev

``protobuf`` and ``grpc`` are also required dependencies. However, on Ubuntu 18.04LTS, the package versions of ``protobuf`` and ``grpc`` are too old for use with ``buildbox``. Therefore manual build and install is necessary.
Please follow the `upstream instructions to install grpc and protobuf <https://github.com/grpc/grpc/blob/master/BUILDING.md>`_.

If you are on Ubuntu 20.04LTS or later, or you're otherwise pointing to a package repository with newer versions, you can install them directly through ``apt``::

    [sudo] apt-get install grpc++ libprotobuf-dev protobuf-compiler-grpc

Compilation
-----------
Once you've installed the dependencies, you can compile ``buildbox`` using the following commands in the buildbox directory::

    mkdir build
    cd build
    cmake -G Ninja .. && ninja

To build ``buildbox`` with GNU Make omit ``-G``::

    mkdir build
    cd build
    cmake .. && make

Optional flag ``-DBUILDBOX_VERSION`` allows setting the version string
that will be reported by ``--version`` of the tools, and that ``recc``
will attach as metadata to its request headers. If none is set, CMake
will try to determine the current git commit and use the short SHA as
a version value. (If that fails, the version will be set to
``CMAKE_PROJECT_VERSION``.)
