/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_DIGESTOPERATIONQUEUE_H
#define INCLUDED_BUILDBOXCASD_DIGESTOPERATIONQUEUE_H

#include <buildboxcommon_protos.h>

#include <functional>
#include <mutex>
#include <tuple>
#include <unordered_map>

namespace buildboxcasd {

class DigestOperationQueue {
    /**
     * This class forces multiple calls to a function for a same digest value
     * be invoked serially.
     *
     * The order of those calls is not guaranteed, only that no more than one
     * `f(digest)` invocation will be executing concurrently with the same
     * `digest`.
     */
  public:
    DigestOperationQueue() {}

    typedef std::function<void(const buildboxcommon::Digest &digest)>
        DigestOperation;

    // Launch the given function associated to a digest value guaranteeing that
    // only one function will run simultaneously for that digest.
    // In the case where a previous invocation for that digest is still
    // running, the function will block until the invocation that is running
    // finishes.
    void runExclusively(const buildboxcommon::Digest &digest,
                        const DigestOperation &function);

    // Default constructor
    ~DigestOperationQueue() = default;

    // Disallowing copies:
    DigestOperationQueue(const DigestOperationQueue &) = delete;
    DigestOperationQueue &operator=(DigestOperationQueue const &) = delete;

    // Disallowing moves
    DigestOperationQueue(DigestOperationQueue &&) = delete;
    DigestOperationQueue &operator=(DigestOperationQueue &&) = delete;

  private:
    // `d_queue` keeps a mutex for each Digest for which a function is
    // currently running.
    // Functions will execute with the mutex for their corresponding digest
    // locked and will unlock it once they are done.
    // A reference counter allows to detect when the last thread waiting on
    // the mutex has finished and therefore the digest entry can be deleted.
    typedef std::pair<std::mutex, unsigned int> QueueEntry;
    std::unordered_map<buildboxcommon::Digest, QueueEntry> d_queue;

    // Exclusive access to `d_queue`.
    std::mutex d_queueMutex;

    // Get a reference to the mutex entry in `queue` for the given digest.
    // If the digest does not have one, create an entry in `d_queue`.
    // Increment the reference counter for that Digest.
    std::mutex &getDigestMutex(const buildboxcommon ::Digest &digest);
};
} // namespace buildboxcasd
#endif
