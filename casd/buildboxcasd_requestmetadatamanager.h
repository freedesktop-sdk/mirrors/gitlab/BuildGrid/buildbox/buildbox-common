#ifndef BUILDBOXCASD_REQUEST_METADATA_MANAGER
#define BUILDBOXCASD_REQUEST_METADATA_MANAGER

#include <buildboxcommon_protos.h>
#include <grpcpp/client_context.h>

namespace buildboxcasd {

using namespace build::bazel::remote::execution::v2;

class RequestMetadataManager {
  public:
    static RequestMetadata getRequestMetadata();
    static void setRequestMetadata(const RequestMetadata &metadata);
    static void attachMetadata(grpc::ClientContext *ctx);
    static void unsetRequestMetadata();

  private:
    thread_local static RequestMetadata d_request_metadata;
    thread_local static bool d_metadata_set;
};

} // namespace buildboxcasd

#endif