
#include <buildboxcasd_requestmetadatamanager.h>
#include <buildboxcommon_requestmetadata.h>

using namespace buildboxcasd;
using namespace build::bazel::remote::execution::v2;

thread_local RequestMetadata RequestMetadataManager::d_request_metadata =
    RequestMetadata::default_instance();
thread_local bool RequestMetadataManager::d_metadata_set = false;

void RequestMetadataManager::setRequestMetadata(
    const RequestMetadata &metadata)
{
    d_request_metadata.CopyFrom(metadata);
    d_request_metadata.mutable_tool_details()->set_tool_name(
        d_request_metadata.tool_details().tool_name() +
        " (via buildbox-casd)");
    d_metadata_set = true;
}

RequestMetadata RequestMetadataManager::getRequestMetadata()
{
    return d_request_metadata;
}

void RequestMetadataManager::attachMetadata(grpc::ClientContext *ctx)
{
    if (d_metadata_set) {
        ctx->AddMetadata(
            buildboxcommon::RequestMetadataGenerator::HEADER_NAME,
            RequestMetadataManager::getRequestMetadata().SerializeAsString());
    }
}

void RequestMetadataManager::unsetRequestMetadata()
{
    d_request_metadata = RequestMetadata::default_instance();
    d_metadata_set = false;
}
