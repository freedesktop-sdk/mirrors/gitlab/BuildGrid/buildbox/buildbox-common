#include <buildboxcasd_localcasservice.h>

INSTANTIATE_TEST_SUITE_P(
    DynamicInstance, CasProxyUnreachableRemoteFixture,
    testing::Combine(testing::Values(DYNAMIC_INSTANCE),
                     testing::Values(FINDMISSINGBLOBS_CACHE_OFF,
                                     FINDMISSINGBLOBS_CACHE_ON),
                     testing::ValuesIn(TEST_INSTANCE_NAMES),
                     testing::Values(NON_THREADED, THREADED)));

GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(CasProxyFixture);
GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(RemoteServerFixture);
