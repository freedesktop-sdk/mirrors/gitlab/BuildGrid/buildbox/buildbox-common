#include "buildboxcasd_proxymodefixture.h"
#include <buildboxcasd_casproxy.h>

INSTANTIATE_TEST_SUITE_P(
    StaticInstanceFmbOff, CasProxyFixture,
    testing::Combine(testing::Values(STATIC_INSTANCE),
                     testing::Values(FINDMISSINGBLOBS_CACHE_OFF),
                     testing::ValuesIn(TEST_INSTANCE_NAMES),
                     testing::Values(NON_THREADED, THREADED)));

GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(
    CasProxyUnReachableRemoteFixture);
GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(
    CasProxyWithFindMissingBlobsCacheFixture);
